import React from 'react';
import { shallow, mount, render } from 'enzyme';

import App from './App';
import ImageList from './component/image-list/image-list';

describe('<App />', () => {

  it('should render only 1 <ImageList /> components', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(ImageList).length).toBe(1);
  });

});
