import React, { Component } from 'react';
import { Provider } from 'react-redux';

// import components
import ImageList from './component/image-list/image-list';

// import store
import configureStore from './store/configure-store';

// style
import './App.css';

// asset
import logo from '../asset/image/logo.svg';

const store = configureStore();

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <Provider store={store}>
          <ImageList />
        </Provider>
      </div>
    );
  }
}

export default App;
