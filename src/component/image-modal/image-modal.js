import React, { Component } from 'react';
import { connect } from 'react-redux';
import Lightbox from 'react-image-lightbox';

class ImageModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  openModal(bool) {
    this.setState({ isOpen: bool });
  }

  render() {
    let photoIndex = 0;
    let images = [this.props.photo.url];

    return (
      <div>
        {
          this.state.isOpen &&
          <Lightbox
              mainSrc={images[photoIndex]}
              nextSrc={images[(photoIndex + 1) % images.length]}
              prevSrc={images[(photoIndex + images.length - 1) % images.length]}
              onCloseRequest={() => this.openModal(false)}
          />
        }
      </div>
    )
  };
}

const mapStateToProps = (state) => {
  return {}
};

export default connect(mapStateToProps, null, null, { withRef: true })(ImageModal);;
