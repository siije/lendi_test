import React from 'react';
import { createStore, Provider } from 'react-redux';
import { shallow, mount, render } from 'enzyme';

import Lightbox from 'react-image-lightbox';
import ImageModal from './image-modal';
import configureStore from '../../store/configure-store';

describe('<ImageModal />', () => {
  let testPhoto = {
    "albumId": 1,
    "id": 1,
    "title": "accusamus beatae ad facilis cum similique qui sunt",
    "url": "http://placehold.it/600/92c952",
    "thumbnailUrl": "http://placehold.it/150/92c952"
  };

  const open = true;
  const close = false;
  const store = configureStore();

  it('should not include <Lightbox /> components', () => {
    const wrapper = shallow(
      <Provider store={store}>
        <ImageModal ref="imageModal" photo={testPhoto} />
      </Provider>
    );
    expect(wrapper.find(Lightbox).length).toBe(0);
  });

});
