import React, { Component } from 'react';

// custome
import ImageModal from '../image-modal/image-modal';

// style
import './image-cell.css';

/*
 * @class ImageCell
 * @extends React.Component
 */
class ImageCell extends Component {

  handleClick(photo) {
    this.refs.imageModal.getWrappedInstance().openModal(true);
  }

  render() {
    return (
      <div className="col-md-3">
        <div className="card image-cell">
          <img className="card-img-top"
               src={ this.props.photo.thumbnailUrl }
               alt="Card"
          />
          <div className="card-block border-block">
            <h5 className="card-title fixed-title">{ this.props.photo.title }</h5>
            <a href="#"
               className="btn btn-primary"
               onClick={this.handleClick.bind(this, this.props.photo)}
            >Show Original</a>
          </div>
        </div>
        <ImageModal ref="imageModal" photo={this.props.photo}/>
      </div>
    );
  }
}

export default ImageCell;
