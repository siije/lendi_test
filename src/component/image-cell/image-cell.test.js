import React from 'react';
import { shallow, mount, render } from 'enzyme';

import ImageCell from './image-cell';
import Lightbox from 'react-image-lightbox';
import ImageModal from '../image-modal/image-modal';

describe('<ImageCell />', () => {
  let testPhoto = {
    "albumId": 1,
    "id": 1,
    "title": "accusamus beatae ad facilis cum similique qui sunt",
    "url": "http://placehold.it/600/92c952",
    "thumbnailUrl": "http://placehold.it/150/92c952"
  };

  it ('Render success', () => {
    const wrapper = shallow(<ImageCell photo={testPhoto} />);
    expect(wrapper.find('img.card-img-top').length).toBe(1);
    expect(wrapper.find(ImageModal).length).toBe(1);
    expect(wrapper.find(Lightbox).length).toBe(0);
  });

});
