import React from 'react';
import { Provider } from 'react-redux';
import { shallow, mount, render } from 'enzyme';

import ImageList from './image-list';
import ImageCell from '../image-cell/image-cell';
import configureStore from '../../store/configure-store';

describe('<ImageList />', () => {

  let originalTimeout = 0;
  let testPhotos = [
    {
      "albumId": 1,
      "id": 1,
      "title": "accusamus beatae ad facilis cum similique qui sunt",
      "url": "http://placehold.it/600/92c952",
      "thumbnailUrl": "http://placehold.it/150/92c952"
    },
    {
      "albumId": 1,
      "id": 3,
      "title": "accusamus beatae ad facilis cum similique qui sunt",
      "url": "http://placehold.it/600/92c952",
      "thumbnailUrl": "http://placehold.it/150/92c952"
    },
    {
      "albumId": 2,
      "id": 3,
      "title": "accusamus beatae ad facilis cum similique qui sunt",
      "url": "http://placehold.it/600/92c952",
      "thumbnailUrl": "http://placehold.it/150/92c952"
    },
  ]

  let mockResponse = {
    ok : true,
    json : function() {
      return testPhotos;
    }
  };

  beforeAll(function() {
    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(mockResponse));
  });

  it ('Render success', (done) => {
    const wrapper = mount(
      <Provider store={configureStore()}>
        <ImageList />
      </Provider>
    );

    setTimeout(() => {
      expect(wrapper.find(ImageCell).length).toBeGreaterThan(1);
      done();
    }, 1000);

  });

});
