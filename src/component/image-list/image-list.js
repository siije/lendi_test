import React, { Component, PropTypes} from 'react';
import { connect } from 'react-redux';

// custome
import { photosFetchData } from '../../actions/photos';
import ImageCell from '../image-cell/image-cell';

// style
import './image-list.css';

// variables
const url = 'https://jsonplaceholder.typicode.com/photos';

/*
 * @class ImageList
 * @extends React.Component
 */
class ImageList extends Component {

  handleClick(albumID){
    this.selected = albumID;
    this.photoSlice = this.props.photos[this.selected];

    this.forceUpdate();
  }

  isActive(albumID) {
    return "list-group-item list-item " + (albumID === this.selected ? "active" : "" );
  }

  componentDidMount() {
    this.props.fetchData(url);
  }

  render() {
    if (this.props.hasErrored) {
      return <p>Sorry! There was an error loading the items</p>;
    }

    if (this.props.isLoading) {
      return <p>Loading…</p>;
    }

    if (this.props.photos.length === 0) {
      return <p>Loading…</p>;
    } else {
      this.albums = Object.keys(this.props.photos);
      this.selected = this.selected || this.albums[0];
      this.photoSlice = this.photoSlice || this.props.photos[this.selected];
    }

    let photosDOM = this.photoSlice.map((photo) => {
      return <ImageCell photo={photo} key={photo.id.toString()}/>
    });

    return (
      <div className='row'>
        <div className="col-md-12"><br/></div>
        <div className="col-md-2">
          <ul className="list-group list">
            {
              this.albums.map((albumID) => {
                return <li className={this.isActive(albumID)}
                          key={albumID.toString()}
                          onClick={this.handleClick.bind(this, albumID)}
                       >
                        Album - {albumID}
                       </li>
              })
            }
          </ul>
        </div>
        <div className="col-md-10 row">
          {photosDOM}
        </div>
      </div>
    );
  }
}

ImageList.propTypes = {
    fetchData : PropTypes.func.isRequired,
    photos : PropTypes.array.isRequired,
    hasErrored : PropTypes.bool.isRequired,
    isLoading : PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    return {
        photos : state.photos,
        hasErrored : state.photosHasErrored,
        isLoading : state.photosIsLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(photosFetchData(url)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageList);
