
export function photosHasErrored(bool) {
  console.log('[Actions] photosHasErrored : ' + bool);
  return {
    type: 'PHOTOS_HAS_ERRORED',
    hasErrored: bool
  };
}

export function photosIsLoading(bool) {
  console.log('[Actions] photosIsLoading : ' + bool);
  return {
    type: 'PHOTOS_IS_LOADING',
    isLoading: bool
  };
}

export function photosFetchDataSuccess(photos) {
  console.log('[Actions] photosFetchDataSuccess');
  return {
    type: 'PHOTOS_FETCH_DATA_SUCCESS',
    photos,
  };
}

export function photosFetchData(url) {
  return (dispatch) => {
    dispatch(photosIsLoading(true));

    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((photos) => {
        let photosJSON = {};

        photos.forEach((photo) => {
          //console.log(photo.albumId);
          if (!photosJSON[photo.albumId]) {
            photosJSON[photo.albumId] = [];
          }

          photosJSON[photo.albumId].push(photo);
        });

        dispatch(photosFetchDataSuccess(photosJSON));
        dispatch(photosIsLoading(false));
      })
      .catch(() => dispatch(photosHasErrored(true)));
  };
}
